package serialMonitor;

import jssc.SerialPortList;

/**
 *
 */
public class SimpleSerialMonitor  {

	public static void main(String[] args) throws Exception {
	    
	    String[] portNames = SerialPortList.getPortNames();
        for (int i = 0; i < portNames.length; i++){
            System.out.println(portNames[i]);
        }
		if (portNames.length != 1){
			System.out.println("args: <CommPortName>");
			System.exit(1);
		} else {
			String comPortName = portNames[0];
			System.out.println("Start monitoring serial port "+portNames[0]+" at 9600 boud rate");
			try {
				SerialMonitor monitor = new SerialMonitor();
				monitor.start(comPortName);							
				Thread.sleep(1000000);
			} catch (InterruptedException ex) {
				ex.printStackTrace();
			}
		}
	}
}